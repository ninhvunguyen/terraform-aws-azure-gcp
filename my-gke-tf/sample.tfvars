# gcp region
region = "us-west1"

# gke cluster name, this is the same name used to create the vpc and subnet
# hence this name must be unique
cluster_name = "devops-gke"
